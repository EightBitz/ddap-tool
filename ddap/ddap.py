#!python3
#
# Implements a command line tool to create and currate a custom asset pack for Dungeon Draft
#
#   https://github.com/Megasploot/Dungeondraft/wiki/Custom-Assets-Guide
#
import argparse
import os
import json
from PIL import ImageDraw, Image, ImageFont

comp_list = ['preview', 'wall', 'tileset', 'light', 'object', 'material',
             'path', 'pattern', 'portal', 'terrain']

default_tags = {
    "tags": {},
    "sets": {}
}

smfont = ImageFont.truetype("Helvetica", 24)
mdfont = ImageFont.truetype("Helvetica", 40)
lgfont = ImageFont.truetype("Helvetica", 80)

parser = argparse.ArgumentParser(prog='ddraft', description='Dungeondraft custom asset tool')
subparsers = parser.add_subparsers(dest='subparser', help='cmd -h')
parser_init = subparsers.add_parser('init', help='init -h', description='Create a skeleton asset pack')
parser_init.add_argument('-a', '--all', action='store_true', help='init all resources with default example seeds')
parser_init.add_argument('pack', help='name of the pack you are creating')
parser_add = subparsers.add_parser('add', help='add -h', description='Add a resource to the pack with default images and json set up')
parser_add.add_argument('pack', help='name of the pack you are creating')
parser_add.add_argument('kind', type=str, choices=comp_list, help='the type of component you want to add')
parser_add.add_argument('name', type=str, nargs='?', default='example', help='the name of the component you want to add')
parser_update = subparsers.add_parser('update', help='update -h', description='Update everything to get it ready for packing')
parser_update.add_argument('kind', type=str, help='name of the component type you want to add')
parser_delete = subparsers.add_parser('delete', help='delete -h', description='delete an asset from the pack files and json')
parser_delete.add_argument('pack', type=str, help='name of the pack you are deleting from')
parser_delete.add_argument('kind', type=str, choices=comp_list, help='the kind of component you want to delete')
parser_delete.add_argument('name', type=str, nargs='?', default='example', help='the name of the component you want to delete')

class Walls:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addwall', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'walls', name+'_wall.png')
        im = Image.new('RGBA', (1024, 62), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 2, 1024, 58), fill='grey', outline=None)
        im.save(oFile, "PNG")
        # now the end graphic
        oFile = os.path.join(self.pack, 'textures', 'walls', name+'_wall_end.png')
        im = Image.new('RGBA', (9, 64), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        shape = [(0,0),(0,64),(6,64),(9,44),(9, 20), (6,0)]
        draw.polygon(shape, fill='grey', outline=None)
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'walls', name+'_wall.png')
        os.remove(oFile)
        oFile = os.path.join(self.pack, 'textures', 'walls', name+'_wall_end.png')
        os.remove(oFile)

class Lights:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addlight', self.pack)
        oFile = os.path.join(self.pack, 'textures', 'lights', name+'_light.png')
        im = Image.new('RGBA', (1024, 1024), color='black')
        draw = ImageDraw.Draw(im)
        draw.ellipse((24, 24, 1000, 1000), fill='white', outline='white') # TODO: make this a gradient with numpy
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'lights', name+'_light.png')
        os.remove(oFile)

class Tilesets:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addtile', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'tilesets', 'simple', name+'_tileset.png')
        im = Image.new('RGBA', (1024, 1024), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        spacing = 1024/4
        for n in range(1, 4      ):
            draw.line(((n*spacing, 0), (n*spacing, 1024)), width=1, fill='black')
            draw.line(((0, n*spacing), (1024, n*spacing)), width=1, fill='black')
        draw.text((10, 20), "Simple\r\nb/w - 16 tiles\r\ntip: create a new layer under this to paint on\r\n"
                    + "delete this layer when done.", font=mdfont, fill='darkgrey')
        im.save(oFile, 'PNG')
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'tilesets', name+'_tileset.png')
        os.remove(oFile)

class Materials:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addmaterial', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'materials', name+'_tile.png')
        im = Image.new('RGBA', (2048, 2048), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 0, 2048, 2048), fill='grey', outline=None)
        draw.text((100, 100), "Texture should be seamless.", font=mdfont)
        im.save(oFile, "PNG")
        # now the end graphic
        oFile = os.path.join(self.pack, 'textures', 'materials', name+'_border.png')
        im = Image.new('RGBA', (2048, 87), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle(((0, 1), (2048, 85)), fill='grey', outline=None)
        draw.text((20, 20), "leave at least 1 px transparent at top and bottom", font=mdfont)
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'materials', name+'_tile.png')
        os.remove(oFile)
        oFile = os.path.join(self.pack, 'textures', 'materials', name+'_border.png')
        os.remove(oFile)

class Objects:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addobject', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'objects', name+'.png')
        im = Image.new('RGBA', (256, 256), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 5, 251, 251), fill=(24, 97, 19), outline=None)
        draw.text((20, 20), "256 pixels per grid unit", font=smfont)
        #os.makedirs(os.path.dirname(oFile)) # TODO: add pack tag to all objects by default
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'objects', name+'.png')
        os.remove(oFile)

class Portals:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('portal', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'portals', name+'_door.png')
        im = Image.new('RGBA', (256, 64), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.polygon((1, 1, 20,1, 127, 10, 234,1, 254, 1, 254, 63, 234, 63, 127, 53, 20, 63, 1, 63), fill='black', outline=(255, 255, 255, 0))
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'portals', name+'_door.png')
        os.remove(oFile)

class Patterns:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('pattern', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'patterns', 'normal', name+'_pattern.png')
        im = Image.new('RGBA', (1024,1024), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 0, 1024, 1024), fill='black')
        for n in range(10, 1024, 10):
            for x in range(10,1024,10):
                draw.point((n,x), fill='white')
        im.save(oFile, "PNG")
        draw.text((20,20),"colorable patterns need to have\r\n fully red areas to recolor", font=mdfont)
        oFile = os.path.join(self.pack, 'textures', 'patterns', 'colorable', name+'_pattern.png')
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'patterns/colorable', name+'_pattern.png')
        os.remove(oFile)
        oFile = os.path.join(self.pack, 'textures', 'patterns/normal', +name+'_pattern.png')
        os.remove(oFile)

class Terrain:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('terrain', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'terrain', name+'_terrain.png')
        im = Image.new('RGBA', (2048, 2048), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 0, 2048, 2048), fill=(24, 97, 19), outline=None)
        draw.text((100, 100), "Texture should be seamless.\r\nalpha channel indicates height for blending", font=lgfont)
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'terrain', name+'_terrain.png')
        os.remove(oFile)

class Paths:
    def __init__(self, pack):
        self.pack = pack
    def add(self, name):
        print('addpath', self.pack, name)
        oFile = os.path.join(self.pack, 'textures', 'paths', name+'_path.png')
        im = Image.new('RGBA', (1024, 256), color=(255, 255, 255, 0))
        draw = ImageDraw.Draw(im)
        draw.rectangle((0, 5, 1024, 251), fill=(24, 97, 19), outline=None)
        draw.text((20, 20), "Texture should be seamless on west/east border.\r\n"
                    "Need to leave at least one pixel border north/south edges", font=mdfont)
        im.save(oFile, "PNG")
    def delete(self, name):
        oFile = os.path.join(self.pack, 'textures', 'paths', name+'_path.png')
        os.remove(oFile)

class Preview:
    def __init__(self, pack):
        self.pack = pack
    def add(self):
        print('addpreview', self.pack)
        oFile = os.path.join(self.pack, 'preview.png')
        im = Image.new('RGBA', (256,320), color=(255, 255,255, 0))
        draw = ImageDraw.Draw(im)
        im.save(oFile, "PNG")
    def delete(self):
        oFile = os.path.join(self.pack, 'preview.png')
        os.remove(oFile)

# TODO: finish self debate on wither to encapsulate the asset classes in the pack class.  python really doesn't OO....
class Pack:
    def __init__(self, pack):
        self.pack = pack

    def init(self):
        print("init", self.pack)
        # TODO: init folder structure
        template = ['data', os.path.join('data', 'walls'), os.path.join('data', 'tilesets'), 'textures',
                os.path.join('textures', 'objects'), os.path.join('textures', 'tilesets'),
                os.path.join('textures', 'tilesets', 'simple'), os.path.join('textures', 'walls'),
                os.path.join('textures', 'lights'), os.path.join('textures', 'materials'),
                os.path.join('textures', 'paths'), os.path.join('textures', 'patterns', 'normal'),
                os.path.join('textures', 'patterns', 'colorable'), os.path.join('textures', 'portals'),
                os.path.join('textures', 'terrain')]
        for x in template:
            d = os.path.join(self.pack, x)
            print(d)
            os.makedirs(d)
    def initall(self, name):
        print("init-all", self.pack)
        self.init()
        Lights(self.pack).add(name)
        Walls(self.pack).add(name)
        Tilesets(self.pack).add(name)
        Materials(self.pack).add(name)
        Objects(self.pack).add(name)
        Portals(self.pack).add(name)
        Patterns(self.pack).add(name)
        Terrain(self.pack).add(name)
        Paths(self.pack).add(name)
        Preview(self.pack).add()
        self.walljson()
        self.tags()
        self.update()
    def update(self):
        print('update', self.pack)
        self.tags()
        self.walljson()
    def tags(self):
        print('tags', self.pack)
        with open(os.path.join(self.pack, 'data', 'default.dungeondraft_tags'), 'w') as outfile:
            json.dump({"tags": {}, "sets": {}}, outfile)


    def walljson(self):
        print('walls', self.pack)
        texdir = os.path.join('textures', 'walls')
        datadir = os.path.join('data', 'walls')
        ws = os.listdir(os.path.join(self.pack, texdir))
        for wall in ws:
            if wall.find('_end.png') == -1:
                print('creating json for: ', wall)
                data = {
                    "path": os.path.join(texdir, wall),
                    "color": "605f58"
                }
                with open(os.path.join(self.pack, os.path.join(datadir, wall.replace('.png', '.dungeondraft_wall'))), 'w') as outfile:
                    json.dump(data, outfile, indent=4, sort_keys=False)

## TODO: update the json files
def main(args):
    print('args: ', args)
    if args.subparser == 'init':
        if args.all:
            Pack(args.pack).initall('example')
        else:
            Pack(args.pack).init()
    elif args.subparser == 'add':
        if args.kind == 'wall':
            Walls(args.pack).add(args.name)
        elif args.kind == 'light':
            Lights(args.pack).add(args.name)
        elif args.kind == 'tileset':
            Tilesets(args.pack).add(args.name)
        elif args.kind == 'material':
            Materials(args.pack).add(args.name)
        elif args.kind == 'object':
            Objects(args.pack).add(args.name)
        elif args.kind == 'portal':
            Portals(args.pack).add(args.name)
        elif args.kind == 'terrain':
            Terrain(args.pack).add(args.name)
        elif args.kind == 'pattern':
            Patterns(args.pack).add(args.name)
        elif args.kind == 'path':
            Paths(args.pack).add(args.name)
        elif args.kind == 'preview':
            Preview(args.pack).add()
    elif args.subparser == 'delete':
        if args.kind == 'wall':
            Walls(args.pack).delete(args.name)
        elif args.kind == 'light':
            Lights(args.pack).delete(args.name)
        elif args.kind == 'tileset':
            Tilesets(args.pack).delete(args.name)
        elif args.kind == 'material':
            Materials(args.pack).delete(args.name)
        elif args.kind == 'object':
            Objects(args.pack).delete(args.name)
        elif args.kind == 'pattern':
            Patterns(args.pack).delete(args.name)
        elif args.kind == 'portal':
            Portals(args.pack).delete(args.name)
        elif args.kind == 'terrain':
            Terrain(args.pack).delete(args.name)
        elif args.kind == 'path':
            Paths(args.pack).delete(args.name)
        elif args.kind == 'preview':
            Preview(args.pack).delete()
    elif args.subparser == 'update':
        Pack(args.pack).update()
    else:
        quit(1)


### MAIN ###
main(parser.parse_args())